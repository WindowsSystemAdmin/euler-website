+++
version="openEuler 1.0 Base"
description=""
status="EOL"
releaseDate="2020/02/11"
releaseNote="https://openeuler.org/zh/docs/1.0_Base/docs/Releasenotes/release_notes.html"
installationGuide="https://openeuler.org/zh/docs/1.0_Base/docs/Installation/installation.html"
EOLDate="2020/03/26"
ISOUrl="https://repo.openeuler.org/openeuler1.0/base/iso/"
+++
openEuler 1.0 Base版本是为了满足特定用户指定场景业务需求发布的版本，在openEuler 1.0 Standard版本发布后，本版本停止服务。

openEuler 1.0 Standard 版本是满足开放场景的标准发行版，规划作为LTS版本，生命周期四年。